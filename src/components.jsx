import { react2angular } from 'react2angular';
import { UIRouterContext } from '@uirouter/react-hybrid';
import { UIView, UISref, UISrefActive } from '@uirouter/react';
import React from 'react';

export class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: [] };
  }

  componentWillMount() {
    this.props.Items.all().then((items) => {
      this.setState({ ...this.state, items });
    });
  }

  render() {
    const list = this.state.items.map((item) => (
      <div class="item" key={item.id}>
        <UISrefActive class="active">
          <UISref to=".detail" params={{ id: item.id }}>
            <a>{item.label}</a>
          </UISref>
        </UISrefActive>
      </div>
    ));
    return (
      <div class="container">
        <div class="list">{list}</div>
        <UIView />
      </div>
    );
  }
}

export class Detail extends React.Component {
  render() {
    console.info('Got:', this.props.item);
    return (
      <div class="detail">
        <h1>Details for {this.props.item.label}</h1>
        <div>
          ID: <b>{this.props.item.id}</b>
        </div>
        <div>
          Label: <b>{this.props.item.label}</b>
        </div>
      </div>
    );
  }
}

export const ListComponent = react2angular(UIRouterContext(List), [], ['Items']);
export const DetailComponent = react2angular(Detail, ['item'], ['$stateParams']);
