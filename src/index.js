import angular from 'angular';
import * as UiRouter from '@uirouter/react-hybrid';
import { List, Detail, ListComponent, DetailComponent } from './components';

angular
  .module('app', ['ui.router', 'ui.router.react.hybrid'])
  .component('list', ListComponent)
  .component('detail', DetailComponent)
  .factory('Items', ['$q', ItemsFactory])
  .config(['$stateProvider', '$urlRouterProvider', routes]);

function routes($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state({
      name: 'list',
      url: '/list',
      component: 'list'
    })
    .state({
      name: 'list.detail',
      url: '/{id}',
      params: {
        id: {
          type: 'int'
        }
      },
      resolve: {
        item: [
          'Items',
          '$stateParams',
          function(Items, $stateParams) {
            return Items.byId($stateParams.id).then((it) => {
              console.info('Resolved:', it);
              return it;
            });
          }
        ]
      },
      component: 'detail'
    })
    .state({
      name: 'reactlist',
      url: '/reactlist',
      component: List,
      resolve: {
        Items: 'Items'
      }
    })
    .state({
      name: 'reactlist.detail',
      url: '/{id}',
      component: Detail,
      params: {
        id: {
          type: 'int'
        }
      },
      resolve: {
        item: [
          'Items',
          '$stateParams',
          function(Items, $stateParams) {
            return Items.byId($stateParams.id).then((it) => {
              console.info('Resolved:', it);
              return it;
            });
          }
        ]
      }
    });

  $urlRouterProvider.when('', '/list').when('/', '/list');
}

function ItemsFactory($q) {
  const items = [];
  for (let id = 1; id <= 10; id++) {
    items.push({
      id,
      label: `Item №\u202F${id}`
    });
  }

  return {
    byId(id) {
      const item = items.find((it) => it.id === id);
      if (item) {
        return $q.resolve(item);
      }
      return $q.reject();
    },

    all() {
      return $q.resolve(items);
    }
  };
}
